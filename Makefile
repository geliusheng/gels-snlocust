.PHONY: all linux clean

all:
	@echo "Usage: $(MAKE) <platform>"
	@echo "  * linux"
	@echo "  * macosx"
	@echo "  * clean"

linux:
	@echo "========== make skynet start =========="
	cd 3rd/skynet;make clean;make linux MALLOC_STATICLIB= SKYNET_DEFINES=-DNOUSE_JEMALLOC
	@echo "========== make skynet end =========="

	@echo "\n\n========== make lua-lfs start =========="
	cd 3rd/lua-lfs;make linux
	@echo "========== make lua-lfs end =========="

	@echo "\n\n========== make lua-curl start =========="
	cd 3rd/lua-curl;make linux
	@echo "========== make lua-curl end =========="

	@echo "\n\n========== make lua-json start =========="
	cd 3rd/lua-json;make
	@echo "========== make lua-json end =========="

macosx:
	@echo "========== make skynet start =========="
	cd 3rd/skynet;make clean;make macosx MALLOC_STATICLIB= SKYNET_DEFINES=-DNOUSE_JEMALLOC
	@echo "========== make skynet end =========="

	@echo "\n\n========== make lua-lfs start =========="
	cd 3rd/lua-lfs;make macosx
	@echo "========== make lua-lfs end =========="

	@echo "\n\n========== make lua-curl start =========="
	cd 3rd/lua-curl;make macosx
	@echo "========== make lua-curl end =========="

	@echo "\n\n========== make lua-json start =========="
	cd 3rd/lua-json;make
	@echo "========== make lua-json end =========="

clean:
	@echo "========== clean skynet start =========="
	cd 3rd/skynet;make cleanall
	@echo "========== clean skynet end =========="

	@echo "\n\n========== clean lua-lfs start =========="
	cd 3rd/lua-lfs;make clean
	@echo "========== clean lua-lfs end =========="

	@echo "\n\n========== clean lua-curl start =========="
	cd 3rd/lua-curl;make clean
	@echo "========== clean lua-curl end =========="

	@echo "\n\n========== clean lua-json start =========="
	cd 3rd/lua-json;make clean
	@echo "========== clean lua-json end =========="
