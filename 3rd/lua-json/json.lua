local json = {}
-- local cjson = require("cjson")  --有问题: local data = {[70012] = 1,};local str = json.encode(data);Log.d("==2342342342=====", str)
local cjson = require("cjson.safe")

function json.encode(var)
    local status, result = pcall(cjson.encode, var)
    if status then 
		return result 
	end
end

function json.decode(text)
    local status, result = pcall(cjson.decode, text)
    if status then 
		return result
	end
end

return json
